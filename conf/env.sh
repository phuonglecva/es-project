#!/usr/bin/env bash
# Java to use
# export JAVA_HOME=/usr/lib/jvm/java

# Main class name, must be set
export APP_MAIN_CLASS=com.aic.netview.parser.NetviewParserApp

# App name, will appear in jps
# export APP_NAME=

# Heap size
export APP_HEAPSIZE=4000

# Extra class path
# export APP_CLASSPATH=

# Extra runtime opt
export REMOTE_DEBUG_OPT="-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=9216"
export APP_OPTS="-XX:+UseConcMarkSweepGC"

# Log
# export APP_LOG_DIR=
# export APP_LOGFILE=
# export APP_ROOT_LOGGER=INFO,DRFA

# Daemonize
# export APP_PID_DIR=/tmp

# Set run mode
if [ -z $RUNMODE ]; then
    export RUNMODE="dev"
fi
