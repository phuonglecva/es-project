package com.phuonglc.esstorage.thread;

import com.phuonglc.esstorage.repository.es.EsRepository;
import com.phuonglc.esstorage.utils.IndexUtils;
import lombok.SneakyThrows;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;


public class DeleteIndicesThread implements Runnable {
    private EsRepository esRepository;
    private static final Logger logger = Logger.getLogger(DeleteIndicesThread.class);

    public DeleteIndicesThread(EsRepository esRepository) {
        this.esRepository = esRepository;
    }


    @SneakyThrows
    @Override
    public void run() {
        String[] allIndices = esRepository.getAll();
        String threadName = Thread.currentThread().getName();
        List<String> indicesBefore = IndexUtils.getIndicesBefore(5);
        List<String> deletedIndices = Arrays.stream(allIndices)
                .filter(index -> index.startsWith("orm_article"))
                .filter(index -> !indicesBefore.contains(index)).collect(Collectors.toList());
        logger.info("Thread name: " + threadName + "\t" + ", found " + (deletedIndices.size()) + " indices to delete");
        if (deletedIndices.size() > 0) {
            esRepository.deleteIndicesByNames(deletedIndices.subList(0, 1));
        }
    }
}
