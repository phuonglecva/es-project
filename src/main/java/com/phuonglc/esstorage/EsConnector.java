package com.phuonglc.esstorage;

import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;

public class EsConnector {
    public static RestHighLevelClient getInstance() {
        return EsConnectorHelper.INSTANCE;
    }
    private static class EsConnectorHelper {
        private static final RestHighLevelClient INSTANCE = new RestHighLevelClient(
                RestClient.builder(
                        new HttpHost("hadoop-master", 9200)
                )
        );
    }
}
