package com.phuonglc.esstorage;

import com.phuonglc.esstorage.config.ApplicationConfig;
import com.phuonglc.esstorage.repository.es.EsRepository;
import com.phuonglc.esstorage.thread.DeleteIndicesThread;
import org.aeonbits.owner.ConfigCache;
import org.apache.log4j.Logger;

import java.util.Arrays;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;


public class EsTestMain {
    private static final String USERNAME = "crawler";
    private static final String PASSWORD = "crawler@2019";
    private static final Logger logger = Logger.getLogger(EsTestMain.class);

    public static void main(String[] args) {
        ApplicationConfig applicationConfig = ConfigCache.getOrCreate(ApplicationConfig.class);
        logger.info("Loaded config: "
                + "time_delay(minutes)="
                + applicationConfig.getThreadDelay()
                + ", es host list="
                + Arrays.toString(applicationConfig.getEsHosts()));
        int threadDelay = applicationConfig.getThreadDelay();
        EsRepository esRepository = EsRepository.newInstance();
        esRepository.initializeConnection(applicationConfig.getEsHosts(), USERNAME, PASSWORD);

        ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(5);
        scheduledExecutorService.scheduleAtFixedRate(new DeleteIndicesThread(esRepository), 0, threadDelay, TimeUnit.MINUTES);
    }
}
