package com.phuonglc.esstorage.config;

import org.aeonbits.owner.Accessible;
import org.aeonbits.owner.Config;
import org.apache.logging.log4j.util.PerformanceSensitive;

import javax.annotation.meta.TypeQualifierDefault;

@Config.Sources("file:conf/application.properties")
public interface ApplicationConfig extends Config, Accessible {

    @Key("es.hosts")
    @DefaultValue("http:localhost:9200")
    @Separator(";")
    String[] getEsHosts();


    @Key("es.thread.delay")
    @DefaultValue("1")
    int getThreadDelay();
}
