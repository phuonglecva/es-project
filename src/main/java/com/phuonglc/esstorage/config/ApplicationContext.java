package com.phuonglc.esstorage.config;

import lombok.Getter;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.redisson.config.ReadMode;
import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.JedisCluster;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.IntStream;

@Getter
public class ApplicationContext {
    private RedissonClient redissonClient;
    private JedisCluster jedisCluster;
    private final String RUNNING_MODE = "cluster";

    public ApplicationContext() {
        init();
    }
    private void init() {
        Set<HostAndPort> haps = new HashSet<>();
        IntStream.range(7000, 7006).asLongStream().forEach(i->{
            haps.add(new HostAndPort("localhost", (int) i));
        });
//        Create pool config
        GenericObjectPoolConfig genericObjectPoolConfig = new GenericObjectPoolConfig();
        genericObjectPoolConfig.setMaxTotal(2);
        genericObjectPoolConfig.setMaxWaitMillis(2000);

        jedisCluster = new JedisCluster(haps, 5000, 5, genericObjectPoolConfig);
        Config redisConfig = new Config();
        String redisConnection = "redis://" + haps.stream().findFirst().get().toString();
        if (StringUtils.equals(RUNNING_MODE, "cluster")) {
            redisConfig.useClusterServers()
                    .setReadMode(ReadMode.MASTER_SLAVE)
                    .setConnectTimeout(60_000)
                    .setTimeout(60_000)
                    .setNodeAddresses(Arrays.asList(redisConnection));
        }
    }

    public static void main(String[] args) {
        ApplicationContext applicationContext = new ApplicationContext();
    }
}
