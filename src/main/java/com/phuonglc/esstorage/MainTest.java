package com.phuonglc.esstorage;

import org.elasticsearch.action.DocWriteResponse;
import org.elasticsearch.action.bulk.*;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.TermQueryBuilder;
import org.elasticsearch.index.reindex.BulkByScrollResponse;
import org.elasticsearch.index.reindex.UpdateByQueryRequest;
import org.elasticsearch.script.Script;
import org.elasticsearch.script.ScriptType;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

public class MainTest {

    public static void main(String[] args) throws IOException, InterruptedException {
        RestHighLevelClient client = EsConnector.getInstance();
//        deleteDocs(client, IntStream.range(1, 8).asLongStream().mapToObj(i-> String.valueOf(i)).collect(Collectors.toList()));
        BulkProcessor bulkProcessor = insertDocs(client, new ArrayList<>());
        System.out.println(bulkProcessor);

        updateDocs(client, "user", "phuonglc");

        client.close();
    }

    public static IndexResponse insertDoc(RestHighLevelClient client, String jsonString) throws IOException {
        IndexRequest indexRequest = new IndexRequest("sample");
        indexRequest.source(jsonString, XContentType.JSON);
        IndexResponse indexResponse = client.index(indexRequest, RequestOptions.DEFAULT);
        return indexResponse;
    }

    public static BulkProcessor insertDocs(RestHighLevelClient client, List<Map> mapList) throws IOException, InterruptedException {

        if (mapList.size() == 0) {
            IntStream.range(0, 10).asLongStream().forEach(i -> {
                HashMap<Object, Object> map = new HashMap<>();
                map.put("user", "phuonglc");
                map.put("postDate", new Date());
                map.put("message", "Im still okie");
                mapList.add(map);
            });
        }

        BulkRequest bulkRequest = new BulkRequest();


        BulkProcessor.Listener listener = new BulkProcessor.Listener() {
            @Override
            public void beforeBulk(long l, BulkRequest bulkRequest) {
                System.out.println("--------------------------------------");
                System.out.println("Before bulking id: [" + l + "]...");
                int numberOfActions = bulkRequest.numberOfActions();
                System.out.println("There are: " + numberOfActions + " actions");
                System.out.println("--------------------------------------");
            }

            @Override
            public void afterBulk(long l, BulkRequest bulkRequest, BulkResponse bulkResponse) {
                System.out.println("--------------------------------------");
                System.out.println("After bulking id: [" + l + "]...");
                long numSuccess = Arrays.stream(bulkResponse.getItems()).filter(item -> !item.isFailed()).count();
                System.out.println("There are: " + numSuccess + " successful actions");
                System.out.println("--------------------------------------");

            }

            @Override
            public void afterBulk(long l, BulkRequest bulkRequest, Throwable throwable) {
                System.out.println("Failed to execute bulk: " + throwable);
            }
        };
        BulkProcessor builder = BulkProcessor.builder(
                (req, blistener) -> client.bulkAsync(req, RequestOptions.DEFAULT, blistener),
                listener
        ).build();

        int i = 0;
        for (Map<String, Object> map : mapList) {
            builder.add(
                    new IndexRequest("posts").id(String.valueOf(i)).source(map)
            );
            i += 1;
        }
        boolean isSucess = builder.awaitClose(30L, TimeUnit.SECONDS);
        if (isSucess) {
            System.out.println("Bulk action sussesfully...");
        }
        builder.close();
        return builder;
    }

    public static BulkResponse deleteDocs(RestHighLevelClient client, List<String> idList) throws IOException {
        BulkRequest request = new BulkRequest();
        for (String s : idList) {
            request.add(new DeleteRequest("posts", s));
        }
        BulkResponse itemResponse = client.bulk(request, RequestOptions.DEFAULT);
        for (BulkItemResponse response : itemResponse) {
            if (response.isFailed()) {
                BulkItemResponse.Failure responseFailure = response.getFailure();
                System.out.println(responseFailure);
            } else {
                DocWriteResponse docWriteResponse = response.getResponse();
                switch (response.getOpType()) {
                    case INDEX:
                    case CREATE:
                        IndexResponse indexResponse = (IndexResponse) docWriteResponse;
                        System.out.println(indexResponse);
                    case UPDATE:
                        UpdateResponse updateResponse = (UpdateResponse) docWriteResponse;
                        System.out.println(updateResponse);
                    case DELETE:
                        DeleteResponse deleteResponse = (DeleteResponse) docWriteResponse;
                        System.out.println(deleteResponse);
                }
            }

        }
        return itemResponse;
    }

    public static BulkByScrollResponse updateDocs(RestHighLevelClient client, String field, String targetValue) throws IOException {
        UpdateByQueryRequest request = new UpdateByQueryRequest("posts");
        request.setConflicts("proceed");
        request.setScript(new Script(ScriptType.INLINE,
                "painless",
                "if(ctx._source.user == 'phuonglc') {ctx._source.user = 'namlh'}"
                , Collections.emptyMap()));
        BulkByScrollResponse bulkByScrollResponse = client.updateByQuery(request, RequestOptions.DEFAULT);
        return bulkByScrollResponse;
    }
}
