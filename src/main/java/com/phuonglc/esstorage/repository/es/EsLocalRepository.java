package com.phuonglc.esstorage.repository.es;

import com.phuonglc.esstorage.utils.IndexUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.nio.client.HttpAsyncClientBuilder;
import org.elasticsearch.ElasticsearchException;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.*;
import org.elasticsearch.client.indices.GetIndexRequest;
import org.elasticsearch.client.indices.GetIndexResponse;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.rest.RestStatus;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class EsLocalRepository {
    private RestHighLevelClient esClient;
    private final int HIT_SIZE = 500;
    private final String topicId = "27950";

    private EsLocalRepository() {
    }

    public static EsLocalRepository newInstance() {
        return new EsLocalRepository();
    }

    public void initializeConnection(String[] hosts, String userName, String password) {
        BasicCredentialsProvider basicCredentialsProvider = new BasicCredentialsProvider();
        basicCredentialsProvider.setCredentials(AuthScope.ANY, new UsernamePasswordCredentials(userName, password));

        HttpHost[] httpHosts = Arrays.stream(hosts).filter(StringUtils::isNotEmpty).map(host -> {
            String protocol = host.split(":")[0];
            String ip = host.split(":")[1];
            int port = Integer.parseInt(host.split(":")[2]);
            return new HttpHost(ip, port, protocol);
        }).toArray(HttpHost[]::new);

        RestClientBuilder restClientBuilder = RestClient.builder(httpHosts).setFailureListener(new RestClient.FailureListener() {
            @Override
            public void onFailure(Node node) {
                System.out.println("elastic node failed: " + node.getHost().toURI());
            }
        }).setHttpClientConfigCallback(new RestClientBuilder.HttpClientConfigCallback() {
            @Override
            public HttpAsyncClientBuilder customizeHttpClient(HttpAsyncClientBuilder httpAsyncClientBuilder) {
                return httpAsyncClientBuilder.setDefaultCredentialsProvider(basicCredentialsProvider);
            }
        });
        this.esClient = new RestHighLevelClient(restClientBuilder);
    }

    public RestHighLevelClient getClient() {
        return esClient;
    }

    public String[] getAll() throws IOException {
        GetIndexRequest getIndexRequest = new GetIndexRequest("*");
        GetIndexResponse getIndexResponse = esClient.indices().get(getIndexRequest, RequestOptions.DEFAULT);
        return getIndexResponse.getIndices();
    }

    public String[] getAllDeleteIndices(int numMonth) throws IOException {
        String[] allIndices = getAll();
        List<String> indicesBefore = IndexUtils.getIndicesBefore(numMonth);
        String[] deleteIndices = Arrays.stream(allIndices).filter(index -> !indicesBefore.contains(index) && index.startsWith("orm_article")).toArray(String[]::new);
        return deleteIndices;
    }

    public HashMap<String, List> getDocsMatched() throws IOException {
        String[] allIndices = getAll();
        List<String> indices = Arrays.stream(allIndices).filter(name -> name.startsWith("orm_article")).collect(Collectors.toList());
        HashMap<String, List> map = new HashMap<>();
        int i = 0;
        for (String index : indices) {
            try {
                List docs = getDocsMatchedInIndex(index);
                map.put(index, docs);
            } catch (IOException e) {
                e.printStackTrace();
            }
            i++;
            if (i > 5) {
                break;
            }
        }

        return map;
    }

    public List getDocsMatchedInIndex(String indexName) throws IOException {
        SearchRequest searchRequest = new SearchRequest(indexName);
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(QueryBuilders.matchAllQuery());
        searchSourceBuilder.size(HIT_SIZE);
        searchRequest.source(searchSourceBuilder);

        SearchResponse searchResponse = esClient.search(searchRequest, RequestOptions.DEFAULT);
        SearchHits hits = searchResponse.getHits();
        List<String> docIds = Arrays.stream(hits.getHits()).map(i -> i.getId()).collect(Collectors.toList());

        HashMap<String, List> map = new HashMap<>();
        HashSet<Object> set = new HashSet<>();
        for (String id : docIds) {
            GetRequest getRequest = new GetRequest(indexName, id);
            GetResponse getResponse = esClient.get(getRequest, RequestOptions.DEFAULT);
            Map<String, Object> sourceAsMap = getResponse.getSourceAsMap();
            if (sourceAsMap.containsKey("topics")) {
                String topic = sourceAsMap.get("topics").toString();
                List<String> topicList = Arrays.stream(topic.replace("[", "").replace("]", "").split(",")).map(s -> s.trim()).collect(Collectors.toList());
                set.addAll(topicList);

                if (topicList.contains(topicId)) {
                    if (!map.containsKey(indexName)) {
                        map.put(indexName, new ArrayList());
                    }
                    List list = map.get(indexName);
                    list.add(id);
                    map.put(indexName, list);
                }
            }
        }
        return map.get(indexName);
    }

    public void insertDocToIndex(String indexName, Map doc) throws IOException {
        IndexRequest request = new IndexRequest(indexName);
        request.id(doc.get("id").toString())
                .source(doc);
        try {
            IndexResponse response = esClient.index(request, RequestOptions.DEFAULT);
        } catch (ElasticsearchException e) {
            if (e.status() == RestStatus.CONFLICT) {
                System.out.println("has some conflic in creating process");
            }
        }
        System.out.println("Inserted ok");

    }
}
