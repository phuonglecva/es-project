package com.phuonglc.esstorage.hbase.conn;

import org.apache.hadoop.hbase.client.Admin;
import org.apache.hadoop.hbase.client.ConnectionFactory;

import java.io.IOException;

public class HbaseConnector {
    public static Admin getInstance(){
        return HbaseHelper.ADMIN;
    }
    private static class HbaseHelper {
        private static Admin ADMIN = null;

        static {
            try {
                ADMIN = ConnectionFactory.createConnection().getAdmin();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }
}
