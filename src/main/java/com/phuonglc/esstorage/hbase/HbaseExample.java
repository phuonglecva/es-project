package com.phuonglc.esstorage.hbase;

import com.phuonglc.esstorage.hbase.conn.HbaseConnector;
import org.apache.directory.api.util.Strings;
import org.apache.hadoop.hbase.*;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;

import java.io.IOException;
import java.util.stream.IntStream;

public class HbaseExample {
    private static final byte[] CF = "cf".getBytes();
    private static final byte[] ATTR = "attr".getBytes();

    public static void main(String[] args) throws IOException {
        Admin admin = HbaseConnector.getInstance();
        int noFamily = 5;
        int noColPerFam = 5;
        String tableName = "sample";
        if (admin.tableExists(TableName.valueOf(tableName))) {
            deleteTable(admin, tableName);
        }
        createTable(admin, tableName, noFamily);
        updateDataToTable(admin, tableName, noFamily, noColPerFam, 100000);
        admin.close();

    }
    public static void deleteTable(Admin admin, String tableName) throws IOException {
        if (admin.tableExists(TableName.valueOf(tableName))) {
            admin.disableTable(TableName.valueOf(tableName));
            admin.deleteTable(TableName.valueOf(tableName));
            System.out.println("Drop table " + tableName + " successfully");
        } else {
            System.out.println("There is no table has name: " + tableName);

        }

    }
    public static void createTable(Admin admin, String tableName, int noFamily) throws IOException {
        if (Strings.isEmpty(tableName)) {
            System.out.println("Please provide table name");
        } else {
            HTableDescriptor descriptor = new HTableDescriptor(TableName.valueOf(tableName));
            for(int i = 0; i < noFamily; i++) {
                descriptor.addFamily(new HColumnDescriptor("cfamily" + i));
            }
            admin.createTable(descriptor);
            System.out.println("Table created");
        }
    }

    public static void updateDataToTable(Admin admin, String tableName, int noFam, int noColPerFam, int numRow) throws IOException {
        Connection connection = admin.getConnection();
        Table table = connection.getTable(TableName.valueOf(tableName));
        for (int i = 0; i < numRow; i++) {
            Put put = new Put(Bytes.toBytes("row" + i));
            for (int j = 0; j < noFam; j++) {
                String famName = "cfamily" + j;
                IntStream.range(0, noColPerFam).asLongStream().forEach(item-> {
                    put.addColumn(Bytes.toBytes(famName), Bytes.toBytes("col" + item),Bytes.toBytes( "value"+ item));
                });
            }
            table.put(put);
        }
        System.out.println("inserted oke");
        table.close();
    }
    public static void getData(Admin admin, String tableName) throws IOException {
        Connection connection = admin.getConnection();
        Table table = connection.getTable(TableName.valueOf(tableName));
        Scan scan = new Scan();
        scan.setCaching(20);

        scan.addFamily(Bytes.toBytes("professional data"));
        ResultScanner scanner = table.getScanner(scan);
        for(Result result = scanner.next(); (result != null); result = scanner.next()) {
            byte[] row = result.getRow();
            System.out.println(Bytes.toString(row));
            System.out.println("=============");
            for(Cell keyValue: result.listCells()) {
                String qualifier = Bytes.toString(CellUtil.cloneQualifier(keyValue));
                String value = Bytes.toString(CellUtil.cloneValue(keyValue));
                System.out.printf("Qualifier: %s, value: %s", qualifier, value);
            }
        }
    }

    public static void getDataByRowId(Admin admin, String tableName, String rowKey) throws IOException {
        Connection connection = admin.getConnection();
        Get theGet = new Get(Bytes.toBytes(rowKey));
        Table table = connection.getTable(TableName.valueOf(tableName));
        Result result = table.get(theGet);
        byte[] value = result.getValue(Bytes.toBytes("professional data"), Bytes.toBytes("salary"));
        System.out.println(Bytes.toString(value));
    }
}
