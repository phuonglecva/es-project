package com.phuonglc.esstorage.utils;


import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class FileUtils {
    public static void writeListToFile(List listItem, String path) throws IOException {
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(path));
        listItem.forEach(item -> {
            try {
                bufferedWriter.write(item.toString()+ "\n");
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        System.out.println("Write successfully...");
        bufferedWriter.close();
    }

}
