package com.phuonglc.esstorage.utils;

import org.elasticsearch.search.DocValueFormat;
import org.joda.time.DateTime;
import org.joda.time.LocalDateTime;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

public class IndexUtils {
    public static List<String> getIndicesBefore(int numMonth) {
        int numDays = numMonth * 30;
        LocalDate now = LocalDate.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyMMdd");
        List<String> listIndices = new ArrayList<>();

        IntStream.range(0, numDays + 1).forEach(i->{
            String index = "orm_article_" + now.minusDays(i).format(formatter);
            listIndices.add(index);
        });
        return listIndices;
    }

//    public static void main(String[] args) {
//        List<String> indicesBefore = IndexUtils.getIndicesBefore(1);
//        indicesBefore.forEach(System.out::println);
//    }
}
