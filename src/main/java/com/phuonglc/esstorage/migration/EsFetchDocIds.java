package com.phuonglc.esstorage.migration;

import com.phuonglc.esstorage.repository.es.EsLocalRepository;
import com.phuonglc.esstorage.repository.es.EsRepository;
import lombok.SneakyThrows;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.BlockingQueue;

public class EsFetchDocIds implements Runnable {
    private EsRepository esRepository;
    private EsLocalRepository esLocalRepository;
    private BlockingQueue indexQueue;
    private BlockingQueue taskQueue;

    public EsFetchDocIds(EsRepository esRepository, EsLocalRepository esLocalRepository, BlockingQueue indexQueue, BlockingQueue taskQueue) {
        this.esRepository = esRepository;
        this.esLocalRepository = esLocalRepository;
        this.indexQueue = indexQueue;
        this.taskQueue = taskQueue;
    }

    @SneakyThrows
    @Override
    public void run() {
      while (true) {
          Object indexName = indexQueue.take();
          List<String> docIds = esRepository.getDocIds(indexName.toString());
          for (String docId : docIds) {
              String jobName = indexName.toString() + "," + docId;
              taskQueue.add(jobName);
              System.out.println(Thread.currentThread().getName() + " added new job to queue <==" + jobName);
              Thread.sleep(100);
          }
      }
    }
}
