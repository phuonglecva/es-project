package com.phuonglc.esstorage.migration;

import com.phuonglc.esstorage.repository.es.EsLocalRepository;
import com.phuonglc.esstorage.repository.es.EsRepository;
import lombok.SneakyThrows;

import java.io.IOException;
import java.util.Arrays;
import java.util.concurrent.BlockingQueue;

public class EsFetchIndices implements Runnable {
    private EsRepository esRepository;
    private EsLocalRepository esLocalRepository;
    private BlockingQueue queue;

    public EsFetchIndices(EsRepository esRepository, EsLocalRepository esLocalRepository, BlockingQueue queue) {
        this.esRepository = esRepository;
        this.esLocalRepository = esLocalRepository;
        this.queue = queue;
    }

    @SneakyThrows
    @Override
    public void run() {
        try {
            String[] allIndices = Arrays.stream(esRepository.getAll()).filter(name->name.startsWith("orm_article_")).toArray(String[]::new);
            BlockingQueue blockingQueue = queue;
            for (String allIndex : allIndices) {
                blockingQueue.add(allIndex);
                System.out.println(Thread.currentThread().getName() + " : " + allIndex + "==>" + blockingQueue.size());
                Thread.sleep(100);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
