package com.phuonglc.esstorage.migration;

import com.phuonglc.esstorage.config.ApplicationConfig;
import com.phuonglc.esstorage.repository.es.EsLocalRepository;
import com.phuonglc.esstorage.repository.es.EsRepository;
import org.aeonbits.owner.ConfigCache;

import javax.lang.model.element.NestingKind;
import javax.swing.plaf.TableHeaderUI;
import java.util.Arrays;
import java.util.concurrent.*;
import java.util.stream.IntStream;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        BlockingQueue<Object> queue = new LinkedBlockingQueue<>();
        ApplicationConfig applicationConfig = ConfigCache.getOrCreate(ApplicationConfig.class);
        System.out.println(applicationConfig);
        String username = "crawler";
        String password = "crawler@2019";

        Arrays.stream(applicationConfig.getEsHosts()).forEach(System.out::println);
        EsRepository esRepository = EsRepository.newInstance();
        esRepository.initializeConnection(applicationConfig.getEsHosts(), username, password);

        EsLocalRepository esLocalRepository = EsLocalRepository.newInstance();
        esLocalRepository.initializeConnection(new String[]{"http:localhost:9200"}, "","");

        LinkedBlockingQueue<Object> indexQueue = new LinkedBlockingQueue<>();
        LinkedBlockingQueue<Object> taskQueue = new LinkedBlockingQueue<>();

        EsFetchIndices esFetchIndices = new EsFetchIndices(esRepository, esLocalRepository, indexQueue);

        ExecutorService executorService = Executors.newFixedThreadPool(10);
        executorService.submit(esFetchIndices);
        IntStream.range(1, 4).forEach(i->{
            executorService.submit(new EsFetchDocIds(esRepository, esLocalRepository, indexQueue, taskQueue));
        });

        IntStream.range(1, 4).forEach(i-> {
            executorService.submit(new Worker(esRepository, esLocalRepository, taskQueue));
        });
//        executorService.awaitTermination(60, TimeUnit.SECONDS);
        executorService.shutdown();
    }
}
