package com.phuonglc.esstorage.migration;

import com.phuonglc.esstorage.repository.es.EsLocalRepository;
import com.phuonglc.esstorage.repository.es.EsRepository;
import lombok.SneakyThrows;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.BlockingQueue;

public class Worker implements Runnable {
    private EsRepository esRepository;
    private EsLocalRepository esLocalRepository;
    private BlockingQueue taskQueue;

    public Worker(EsRepository esRepository, EsLocalRepository esLocalRepository, BlockingQueue taskQueue) {
        this.esRepository = esRepository;
        this.esLocalRepository = esLocalRepository;
        this.taskQueue = taskQueue;
    }

    @SneakyThrows
    @Override
    public void run() {
        while (true) {
            String task = taskQueue.take().toString();
            String indexName = task.split(",")[0];
            String docId = task.split(",")[1];

            Map<String, Object> doc = esRepository.getDocByIdAndIndex(indexName, docId);
            esLocalRepository.insertDocToIndex(indexName, doc);
            System.out.println(Thread.currentThread().getName() + "migrated to local index " + indexName + "," + docId);
        }
    }
}
