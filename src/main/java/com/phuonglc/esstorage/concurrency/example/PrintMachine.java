package com.phuonglc.esstorage.concurrency.example;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

public class PrintMachine implements Runnable {
    @Override
    public void run() {
        IntStream.range(1, 100).forEach(System.out::println);
        try {
            Thread.sleep(3);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws InterruptedException {
        ExecutorService executorService = Executors.newFixedThreadPool(2);
        executorService.submit(new PrintMachine());
        executorService.submit(new Runnable() {
            @Override
            public void run() {
                IntStream.range(1, 100).forEach(i-> {
                    System.out.println(Thread.currentThread().getName() + ":" + i);
                    try {
                        Thread.sleep(10);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                });
            }
        });

        executorService.awaitTermination(10, TimeUnit.SECONDS);
        executorService.shutdown();
    }
}
