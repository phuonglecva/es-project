package com.phuonglc.esstorage.concurrency;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;

public class MainTest {
    static int BOUND = 100;
    static int N_PRODUCER = 4;
    static int N_CONSUMERS = 4;
    static int poisonPill = Integer.MAX_VALUE;
    static int poisonPillPerProducer = N_CONSUMERS  /N_PRODUCER;
    static int mod = N_CONSUMERS % N_PRODUCER;

    public static void main(String[] args) throws IOException {
        BlockingQueue<Integer> queue = new LinkedBlockingDeque<>(BOUND);
        for(int i = 0; i  < N_PRODUCER; i++) {
            new Thread(new Producer(queue, poisonPill, poisonPillPerProducer)).start();
        }
        for(int i = 0; i < N_CONSUMERS; i ++) {
            new Thread(new Consumer(queue, poisonPill)).start();
        }
        new Thread(new Producer(queue, poisonPill, poisonPillPerProducer + mod)).start();
    }
}
