package com.phuonglc.esstorage.concurrency;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.IntStream;

public class Producer implements Runnable{
    private final BlockingQueue<Integer> numbersQueue;
    private final int poisonPill;
    private final int poisonPillPerProducer;

    public Producer(BlockingQueue<Integer> numbersQueue, int poisonPill, int poisonPillPerProducer) {
        this.numbersQueue = numbersQueue;
        this.poisonPill = poisonPill;
        this.poisonPillPerProducer = poisonPillPerProducer;
    }

    @Override
    public void run() {
        generateNumber();
    }

    private void generateNumber() {
        IntStream.range(1, 100).forEach(i-> {
            numbersQueue.add(ThreadLocalRandom.current().nextInt(100));
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        IntStream.range(0, this.poisonPillPerProducer).forEach(i->this.numbersQueue.add(poisonPill));
    }
}
